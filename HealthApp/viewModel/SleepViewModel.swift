//
//  viewModel.swift
//  HealthApp
//
//  
//

import Combine
import Foundation
import HealthKit

class SleepViewModel: ObservableObject {
    @Published private var sleepService = SleepService()
    @Published private(set) var historyModel = SleepModel()
    @Published var sleepHistory = [SleepModel]()
    
    init() {
        sleepConfiguration()
    }

    private func healthStore() {
        sleepService.retrieveSleepWithAuth()
    }

    private func getTotalSleptHours() {
        sleepService.getTotalSleptHours {
            [self] result ->Void in
            DispatchQueue.main.async {
                self.historyModel.totalSleptHours = result
            }
        }
    }

    private func getInBedHours() {
        sleepService.getInBedSleep {
            result ->Void in
            DispatchQueue.main.async {
                self.historyModel.inBedHours = result
            }
        }
    }

    private func getAwakeHours() {
        sleepService.getAwakeSleep {
            result ->Void in
            
            DispatchQueue.main.async {

                self.historyModel.awakeHours = result
            }
        }
    }

    private func getAsleepHours() {
        sleepService.getAsleepHours {
            result ->Void in
            DispatchQueue.main.async {
                self.historyModel.asleepHours = result
            }
        }
    }
  private func sleepConfiguration(){
        self.healthStore()
        self.getTotalSleptHours()
        self.getInBedHours()
        self.getAwakeHours()
        self.getAsleepHours()
        self.testData()
    }
    private func testData(){
        sleepHistory.append(SleepModel(totalSleptHours: "6h", asleepHours: "5h 30", inBedHours: "25m", awakeHours: "5"))
    }
    
}

