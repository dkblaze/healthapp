//
//  HeartViewModel.swift
//  HealthApp
//
//  
//

import SwiftUI
import Combine

class HeartViewModel: ObservableObject {
    @Published private var heartService = HeartService()
    @Published private(set) var heartModel = HeartModel()
    
    init() {
        sleepConfiguration()
    }

    private func healthStore() {
        heartService.retrieveHeartRateWithAuth()
    }

    private func getVo2() {
        heartService.getvo2Rate() {
            [self] result ->Void in
            DispatchQueue.main.async {
                self.heartModel.vo2 = result
            }
        }
    }

    private func getRestingHeartRate() {
        heartService.getRestingHeartRate() {
            result ->Void in
            DispatchQueue.main.async {
                self.heartModel.restingHeartRate = result
            }
        }
    }

    private func getHeartRate() {
        heartService.getUserHeartRate{
            result ->Void in
            
            DispatchQueue.main.async {

                self.heartModel.heartRate = result
            }
        }
    }


  private func sleepConfiguration(){
    self.heartService.retrieveHeartRateWithAuth()
    self.getVo2()
    self.getHeartRate()
    self.getRestingHeartRate()
       
    }
    
}

