//
//  HistoryModel.swift
//  HealthApp
//
//  
//

import Foundation

struct SleepModel: Identifiable {
    var id = UUID()
    var totalSleptHours: String
    var asleepHours: String
    var inBedHours: String
    var awakeHours: String
    
    init(totalSleptHours: String = "",asleepHours: String = "",inBedHours: String = "",awakeHours: String = ""){
        self.totalSleptHours = totalSleptHours
        self.asleepHours = asleepHours
        self.inBedHours = inBedHours
        self.awakeHours = awakeHours
    }
}
