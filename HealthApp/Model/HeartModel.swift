//
//  HeartModel.swift
//  HealthApp
//
//  
//

import Foundation


struct HeartModel: Identifiable {
    var id = UUID()
    var heartRate: String
    var restingHeartRate: String
    var vo2: String
   
    
    init(heartRate: String = "",restingHeartRate: String = "",vo2: String = ""){
        self.heartRate = heartRate
        self.restingHeartRate = restingHeartRate
        self.vo2 = vo2
    }
}
