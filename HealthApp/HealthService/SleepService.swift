//
//  HealthModel.swift
//  HealthApp
//
//  
//

import Foundation
import HealthKit

class SleepService {
    var healthStore: HKHealthStore?
    
    init() {
        if HKHealthStore.isHealthDataAvailable() {
            self.healthStore = HKHealthStore()
        }
    }
    
    func retrieveSleepWithAuth() {
        let typestoRead = Set([
            HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)!
        ])
        let typestoShare = Set([
            HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)!
        ])
    
        healthStore?.requestAuthorization(toShare: typestoShare, read: typestoRead) { (success, error) -> Void in
            if success == false {
                print("Authorization denied  --> \(String(describing: error?.localizedDescription))")
            }
            else {
                print("Authorization completed for Sleep Service")
            }
        }
    }
    
    func getTotalSleptHours(completion: @escaping (String) -> Void) {
        let endDate = Date()
        let startDate = endDate.addingTimeInterval(-1.0 * 60.0 * 60.0 * 28.0)
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: HKQueryOptions.strictEndDate)
        
        if let sleepType = HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis) {
            let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
            let query = HKSampleQuery(sampleType: sleepType, predicate: predicate, limit: 0, sortDescriptors: [sortDescriptor]) { (_, tmpResult, error) -> Void in
                
                if error != nil { return }
                var totalSeconds: Double = 0
                if let result = tmpResult {
                    for item in result {
                        if let sample = item as? HKCategorySample {
                            let timeInterval = sample.endDate.timeIntervalSince(sample.startDate)
                            totalSeconds = totalSeconds + timeInterval
                        }
                    }
                }
                let totalSleptHours =
                    String(Int(totalSeconds / 3600)) + "h " +
                    String(Int(totalSeconds.truncatingRemainder(dividingBy: 3600) / 60)) + "m " +
                    String(Int(totalSeconds.truncatingRemainder(dividingBy: 3600)
                            .truncatingRemainder(dividingBy: 60))) + "s"
                completion(totalSleptHours)
            }
            
            healthStore?.execute(query)
        }
    }
    
    func getAsleepHours(completion: @escaping (String) -> Void) {
        let healthStore = HKHealthStore()
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        let endDate = Date()
        let startDate = endDate.addingTimeInterval(-1.0 * 60.0 * 60.0 * 24.0)
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: [])
        
        let sleepQuery = HKSampleQuery(
            sampleType: HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)!,
            predicate: predicate,
            limit: 0,
            sortDescriptors: [sortDescriptor]) { (_, results, error) -> Void in
            
                if error != nil { return }
                var minutesSleepAggr = 0.0
                if let result = results {
                    for item in result {
                        if let sample = item as? HKCategorySample {
                            if sample.value == HKCategoryValueSleepAnalysis.asleep.rawValue, sample.startDate >= startDate {
                                let sleepTime = sample.endDate.timeIntervalSince(sample.startDate)
                                minutesSleepAggr += sleepTime
                            }
                        }
                    }
                    if minutesSleepAggr / 3600 > 1 {
                        let sleptHours = String(Int(minutesSleepAggr / 3600)) + "h " +
                            String(Int(minutesSleepAggr.truncatingRemainder(dividingBy: 3600) / 60)) + "m " +
                            String(Int(minutesSleepAggr.truncatingRemainder(dividingBy: 3600)
                                    .truncatingRemainder(dividingBy: 60))) + "s"
                        completion(sleptHours)
                    }
                    else {
                        let sleptHours =
                            String(Int(minutesSleepAggr.truncatingRemainder(dividingBy: 3600) / 60)) + "m " +
                            String(Int(minutesSleepAggr.truncatingRemainder(dividingBy: 3600)
                                    .truncatingRemainder(dividingBy: 60))) + "s"
                        completion(sleptHours)
                    }
                }
        }
        healthStore.execute(sleepQuery)
    }
    
    func getInBedSleep(completion: @escaping (String) -> Void) {
        let healthStore = HKHealthStore()
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        let endDate = Date()
        let startDate = endDate.addingTimeInterval(-1.0 * 60.0 * 60.0 * 24.0)
        
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: [])

        let sleepQuery = HKSampleQuery(
            sampleType: HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)!,
            predicate: predicate,
            limit: 0,
            sortDescriptors: [sortDescriptor]) { (_, results, error) -> Void in
    
                if error != nil { return }
                var minutesSleepAggr = 0.0
                if let result = results {
                    for item in result {
                        if let sample = item as? HKCategorySample {
                            if sample.value == HKCategoryValueSleepAnalysis.inBed.rawValue, sample.startDate >= startDate {
                                let sleepTime = sample.endDate.timeIntervalSince(sample.startDate)
                                minutesSleepAggr += minutesSleepAggr + sleepTime
                            }
                        }
                    }
                    
                    if minutesSleepAggr / 3600 > 1 {
                        let sleptHours = String(Int(minutesSleepAggr / 3600)) + "h " +
                            String(Int(minutesSleepAggr.truncatingRemainder(dividingBy: 3600) / 60)) + "m " +
                            String(Int(minutesSleepAggr.truncatingRemainder(dividingBy: 3600)
                                    .truncatingRemainder(dividingBy: 60))) + "s"
                        completion(sleptHours)
                    }
                    else {
                        let sleptHours =
                            String(Int(minutesSleepAggr.truncatingRemainder(dividingBy: 3600) / 60)) + "m " +
                            String(Int(minutesSleepAggr.truncatingRemainder(dividingBy: 3600)
                                    .truncatingRemainder(dividingBy: 60))) + "s"
                        completion(sleptHours)
                    }
                }
        }
        healthStore.execute(sleepQuery)
    }
    
    func getAwakeSleep(completion: @escaping (String) -> Void) {
        let healthStore = HKHealthStore()
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierEndDate, ascending: false)
        let endDate = Date()
        let startDate = endDate.addingTimeInterval(-1.0 * 60.0 * 60.0 * 24.0)
        
        let predicate = HKQuery.predicateForSamples(withStart: startDate, end: endDate, options: [])

        let sleepQuery = HKSampleQuery(
            sampleType: HKObjectType.categoryType(forIdentifier: HKCategoryTypeIdentifier.sleepAnalysis)!,
            predicate: predicate,
            limit: 0,
            sortDescriptors: [sortDescriptor]) { (_, results, error) -> Void in
    
                if error != nil { return }
                var minutesSleepAggr = 0.0
                if let result = results {
                    for item in result {
                        if let sample = item as? HKCategorySample {
                            if sample.value == HKCategoryValueSleepAnalysis.awake.rawValue, sample.startDate >= startDate {
                                let sleepTime = sample.endDate.timeIntervalSince(sample.startDate)
                                
                                minutesSleepAggr += sleepTime
                            }
                        }
                    }
                    if minutesSleepAggr / 3600 > 1 {
                        let awakeHours = String(Int(minutesSleepAggr / 3600)) + "h " +
                            String(Int(minutesSleepAggr.truncatingRemainder(dividingBy: 3600) / 60)) + "m " +
                            String(Int(minutesSleepAggr.truncatingRemainder(dividingBy: 3600)
                                    .truncatingRemainder(dividingBy: 60))) + "s"
                        completion(awakeHours)
                    }
                    else {
                        let awakeHours =
                            String(Int(minutesSleepAggr.truncatingRemainder(dividingBy: 3600) / 60)) + "m " +
                            String(Int(minutesSleepAggr.truncatingRemainder(dividingBy: 3600)
                                    .truncatingRemainder(dividingBy: 60))) + "s"
                        completion(awakeHours)
                    }
                }
        }
        healthStore.execute(sleepQuery)
    }
}
