//
//  HeartModel.swift
//  HealthApp
//
//  
//

import Foundation
import HealthKit

class HeartService: ObservableObject {
    var healthStore: HKHealthStore?

    init() {
        if HKHealthStore.isHealthDataAvailable() {
            self.healthStore = HKHealthStore()
        }
    }
    
    func retrieveHeartRateWithAuth() {
        let typestoRead = Set([
            HKObjectType.quantityType(forIdentifier: .heartRate)!,
            HKObjectType.quantityType(forIdentifier: .restingHeartRate)!,
            HKObjectType.quantityType(forIdentifier: .vo2Max)!
        ])
        healthStore?.requestAuthorization(toShare: typestoRead, read: typestoRead) { (success, error) -> Void in
            if success == false {
                print("Authorization denied  --> \(String(describing: error?.localizedDescription))")
            } else {
                print("Authorization completed for Heart Service")
            }
        }
    }

    func getUserHeartRate(completion: @escaping (String) -> Void) {
        guard let restingHeartRateSampleType = HKSampleType.quantityType(forIdentifier: .heartRate) else {
            return
        }
        let mostRecentPredicate = HKQuery.predicateForSamples(withStart: Date.distantPast,
                                                              end: Date(),
                                                              options: .strictEndDate)
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        let sampleQuery = HKSampleQuery(sampleType: restingHeartRateSampleType,
                                        predicate: mostRecentPredicate,
                                        limit: HKObjectQueryNoLimit,
                                        sortDescriptors:
                                        [sortDescriptor]) { _, samples, _ in
            DispatchQueue.main.async {
                guard let samples = samples,
                    let mostRecentSample = samples.first as? HKQuantitySample
                else {
                    print("getUserRestingHeartRate sample is missing")
                    return
                }
                let currentHeartRate = String(mostRecentSample.quantity.doubleValue(for: HKUnit(from: "count/s")) * 60.0)
                completion(currentHeartRate)
            }
        }
        HKHealthStore().execute(sampleQuery)
    }

    func getRestingHeartRate(completion: @escaping (String) -> Void) {
        guard let restingHeartRateSampleType = HKSampleType.quantityType(forIdentifier: .restingHeartRate) else {
            print("Resting Heart Rate Sample Type is no longer available in HealthKit")
            return
        }
        let mostRecentPredicate = HKQuery.predicateForSamples(withStart: Date.distantPast,
                                                              end: Date(),
                                                              options: .strictEndDate)
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        let sampleQuery = HKSampleQuery(sampleType: restingHeartRateSampleType,
                                        predicate: mostRecentPredicate,
                                        limit: HKObjectQueryNoLimit,
                                        sortDescriptors:
                                        [sortDescriptor]) { _, samples, _ in
            DispatchQueue.main.async {
                guard let samples = samples,
                    let mostRecentSample = samples.first as? HKQuantitySample
                else {
                    print("getUserRestingHeartRate sample is missing")
                    return
                }
                let restingHeartRate = String(mostRecentSample.quantity.doubleValue(for: HKUnit(from: "count/s")) * 60.0)
                completion(restingHeartRate)
            }
        }
        HKHealthStore().execute(sampleQuery)
    }

    func getvo2Rate(completion: @escaping (String) -> Void) {
        guard let restingHeartRateSampleType = HKSampleType.quantityType(forIdentifier: .vo2Max) else {
            print("Resting Heart Rate Sample Type is no longer available in HealthKit")
            return
        }
        let mostRecentPredicate = HKQuery.predicateForSamples(withStart: Date.distantPast,
                                                              end: Date(),
                                                              options: .strictEndDate)
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate, ascending: false)
        let sampleQuery = HKSampleQuery(sampleType: restingHeartRateSampleType,
                                        predicate: mostRecentPredicate,
                                        limit: HKObjectQueryNoLimit,
                                        sortDescriptors:
                                        [sortDescriptor]) { _, samples, _ in
            DispatchQueue.main.async {
                guard let samples = samples,
                    let mostRecentSample = samples.first as? HKQuantitySample
                else {
                    print("getUserRestingHeartRate sample is missing")
                    return
                }
                let vo2 = String(mostRecentSample.quantity.doubleValue(for: HKUnit(from: "ml/kg*min")))
                completion(vo2)
            }
        }
        HKHealthStore().execute(sampleQuery)
    }
}
