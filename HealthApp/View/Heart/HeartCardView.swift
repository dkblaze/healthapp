//
//  HeartCardView.swift
//  HealthApp
//
//  
//

import SwiftUI

struct HeartCardView: View {
    var title: String
    var content: String
    var body: some View {
        VStack{
            Text(title)
                .foregroundColor(.white)
                .font(.system(.title, design: .rounded))
                .fontWeight(.heavy)
                .padding()
            Text(content)
                .foregroundColor(.white)
                .font(.system(.headline, design: .rounded))
                .fontWeight(.heavy)
                .padding(.bottom)
        }
        .frame(width: UIScreen.main.bounds.width/3 , height: UIScreen.main.bounds.height/6, alignment: .center)
        .padding()
        .background(Color("LightDark"))
        .cornerRadius(30)
    }
}

