//
//  HeartView.swift
//  HealthApp
//
//  
//

import SwiftUI

struct HeartView: View {
    @ObservedObject var heart = HeartViewModel()
    @State var isOn: Bool = false

    var body: some View {
        ZStack {
            Color("Dark").edgesIgnoringSafeArea(.all)

            VStack {
                Image("working")
                    .resizable()
                    .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height/3 + 50, alignment: .center)
                    .cornerRadius(25)
                    .position(x: UIScreen.main.bounds.height/5 + 30, y: UIScreen.main.bounds.height/6 + 10)
            }
            .ignoresSafeArea(.all)

            VStack {
                Spacer()
                    .frame(height: UIScreen.main.bounds.height/4)
                Text("Heart Information")
                    .foregroundColor(.white)
                    .font(.system(.largeTitle, design: .rounded))
                    .fontWeight(.heavy)
                    .padding(.trailing, UIScreen.main.bounds.width/3 + 45)
                    .lineLimit(3)
                Spacer()
                    .frame(height: UIScreen.main.bounds.height/10, alignment: /*@START_MENU_TOKEN@*/ .center/*@END_MENU_TOKEN@*/)
                HStack(spacing: 20) {
                    HeartCardView(title: "Current", content: heart.heartModel.heartRate + "count/min")
                    HeartCardView(title: "Resting", content: heart.heartModel.restingHeartRate + "count/min")
                }
                .padding(.bottom, 25)
                HStack(spacing: 20) {
                    HeartCardView(title: "VO2", content: heart.heartModel.vo2 + "mL/Kg*min")
                    HeartCardView(title: "State", content: "normal")
                }
            }.position(x: UIScreen.main.bounds.width/2, y: UIScreen.main.bounds.height/2)
        }
    }
}

struct HeartView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            HeartView()
        }
    }
}
