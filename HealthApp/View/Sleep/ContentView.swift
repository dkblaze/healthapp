//
//  ContentView.swift
//  HealthApp
//
//  
//

import Combine
import SwiftUI

struct SleepView: View {
    @ObservedObject var sleep = SleepViewModel()
    @State var isOn: Bool = false

    var body: some View {
        ZStack {
            Color("Dark").edgesIgnoringSafeArea(.all)

            VStack {
                Image("sleep")
                    .resizable()
                    .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height/3 + 50, alignment: .center)
                    .cornerRadius(25)
                    .position(x: UIScreen.main.bounds.height/5 + 30, y: UIScreen.main.bounds.height/6 + 10)

                VStack {
                    Spacer()
                        .frame(height: UIScreen.main.bounds.height/35)

                    Text("Last night")
                        .foregroundColor(.white)
                        .font(.system(.largeTitle, design: .rounded))
                        .fontWeight(.heavy)
                        .padding(.trailing, UIScreen.main.bounds.width/3 + 45)
                        .padding(.bottom, 25)

                    HStack(alignment: .center, spacing: 30) {
                        CardView(title: "Asleep", content: sleep.historyModel.asleepHours)
                        CardView(title: "In Bed", content: sleep.historyModel.inBedHours)
                    }.padding()
                    HStack(alignment: .center, spacing: 30) {
                        CardView(title: "Awake", content: sleep.historyModel.awakeHours)
                        CardView(title: "Quality", content: sleep.historyModel.totalSleptHours)
                    }
                    Spacer()
                        .frame(height: 30)
                }

                .padding(.trailing)
                ContentHeader(isOn: $isOn, sleep: sleep)
                if isOn {
                    ContentCellView(hours: sleep.sleepHistory[0].totalSleptHours)
                }

            }.ignoresSafeArea(.all)
        }
    }
}

struct SleepView_Previews: PreviewProvider {
    static var previews: some View {
        SleepView()
    }
}
