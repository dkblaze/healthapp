//
//  CardView.swift
//  HealthApp
//
//
//

import SwiftUI

struct CardView: View {
    var title: String
    var content: String
    var body: some View {
        VStack(alignment: .center) {
            Text(title)
                .foregroundColor(.white)
                .font(.system(.title, design: .rounded))
                .fontWeight(.heavy)
                .padding()
            Text(content)
                .foregroundColor(.white)
                .font(.system(.headline, design: .rounded))
                .fontWeight(.heavy)
                .padding(.bottom)
        }

        .padding()
        .background(Color("LightDark"))
        .cornerRadius(30)
    }
}
