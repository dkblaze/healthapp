//
//  ContentCellView.swift
//  HealthApp
//
//  
//

import SwiftUI

struct ContentCellView: View {
    var hours: String
    var body: some View {
        HStack(spacing: 10) {
            Image(systemName: "sleep")
                .font(.title)
                .foregroundColor(Color.white)

            Text("Yesterday")
                .font(.system(.headline, design: .rounded))
                .fontWeight(.heavy)
                .foregroundColor(.white)
                .padding(.bottom, 5)
                   
            Spacer()
                .frame(width: 130)

            Text(hours)
                .font(.system(.headline, design: .rounded))
                .fontWeight(.heavy)
                .foregroundColor(.white)
                .padding(.bottom, 5)
            Spacer()
        }
        .padding(5)
        .background(Color("LightRed"))
        .cornerRadius(25)
        .padding(.horizontal, UIScreen.main.bounds.width/20)
        .padding(.bottom, 5)
    }
}
