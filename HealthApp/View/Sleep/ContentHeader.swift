//
//  ContentHeader.swift
//  HealthApp
//
//  
//

import SwiftUI

struct ContentHeader: View {
    @Binding var isOn: Bool
    @ObservedObject var sleep: SleepViewModel

    var body: some View {
        VStack {
            HStack {
                Text("Recent sleep")
                    .font(.system(.title2, design: .rounded))
                    .fontWeight(.heavy)
                    .foregroundColor(Color.white)
                Spacer()
            }
            .padding(.bottom, 10)

            HStack {
                Group {
                    Button(action: {}, label: {
                        Text("1d ago")
                            .font(.system(.subheadline, design: .rounded))
                            .fontWeight(.heavy)
                            .padding(8)
                            .padding(.horizontal, 20)
                            .background(Color("Orange"))
                            .onTapGesture {
                                self.isOn.toggle()
                                addTask()
                            }

                    })
                    Text("7d ago")
                        .font(.system(.subheadline, design: .rounded))
                        .fontWeight(.heavy)
                        .padding(8)
                        .padding(.horizontal, 20)
                        .background(Color("LightDark"))
                        .onTapGesture {
                            self.isOn.toggle()
                            addTask()
                        }

                    Text("All days")
                        .font(.system(.subheadline, design: .rounded))
                        .fontWeight(.heavy)
                        .padding(8)
                        .padding(.horizontal, 20)
                        .background(Color("LightDark"))
                        .onTapGesture {
                            self.isOn.toggle()
                        }
                }
                .font(.system(.subheadline, design: .rounded))
                .foregroundColor(.white)
                .cornerRadius(15)

                Spacer()
            }
        }
        .padding(.leading, UIScreen.main.bounds.width / 10 - 15)
        .padding(.bottom, 15)
    }

    func addTask() {
        sleep.sleepHistory.append(sleep.historyModel)
    }
}
