//
//  HealthAppApp.swift
//  HealthApp
//
//  Created by Chris on 10/10/2020.
//

import SwiftUI

@main
struct HealthAppApp: App {
    @State var index = 0
    var body: some Scene {
        WindowGroup {
            TabView(selection: $index){
                        SleepView()
                            .tabItem {
                                Image(systemName: "sleep")
                                    
                            }
                            .tag(0)
                        HeartView()
                            .tabItem { Image(systemName: "sleep") }
                            .tag(1)
                
                
            }
            
            .tabViewStyle(PageTabViewStyle(indexDisplayMode: .automatic))
            .indexViewStyle(PageIndexViewStyle(backgroundDisplayMode: .always))
            .ignoresSafeArea(.all)
            
            

        }
        
        
    }
}
